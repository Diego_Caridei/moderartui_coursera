package com.example.diegocaridei.moderartui;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SeekBar;
import android.widget.TextView;
import  android.app.AlertDialog;
import  android.net.Uri;



public class MyActivity extends Activity {


    TextView viola;
    TextView fucsia;
    TextView  rosso;
    TextView bianco;
    TextView blue;
    SeekBar progress;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        viola=(TextView)findViewById(R.id.textViewViola);
        fucsia=(TextView)findViewById(R.id.textViewFucsia);
        rosso=(TextView) findViewById(R.id.textViewRossa);
        bianco=(TextView)findViewById(R.id.textViewGrigia);
        blue=(TextView)findViewById(R.id.textViewBlue);
        progress=(SeekBar)findViewById(R.id.progre);


        progress.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            Integer purple,fuc,red,bl = 0;

            @Override

            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                purple= 0xFF5E68AD+i+100;
                fuc=0xFFCC3D8A+i+100;
                red=0xFF941722+i+100;
                bl=0xFF252F89+i+100;


                viola.setBackgroundColor(purple);
                fucsia.setBackgroundColor(fuc);
                rosso.setBackgroundColor(red);
                blue.setBackgroundColor(bl);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {

            showDialog(MyActivity.this,"Inspired by the works of artistss such as Piet Mondrian","Click below to learn more");


            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public void showDialog(Activity activity, String title, CharSequence message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        if (title != null) builder.setTitle(title);

        builder.setMessage(message);
        builder.setPositiveButton("Visit MOMA",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("http://www.moma.org"));
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Not Now", null);
        builder.show();
    }


}
